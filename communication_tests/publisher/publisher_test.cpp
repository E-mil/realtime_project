#include <iostream>
#include <chrono>
#include <time.h>
#include <unistd.h>
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/int64.hpp"
#include "test_messages/msg/timestamp_msg.hpp"

#define PUBLISH_FREQUENCY 10ms
#define NBR_OF_MESSAGES 1000

using namespace std::chrono_literals;

int main(int argc, char** argv) {
    rclcpp::init(argc, argv);

    rmw_qos_profile_t qos_profile = rmw_qos_profile_default;
    qos_profile.reliability = RMW_QOS_POLICY_RELIABILITY_BEST_EFFORT;
    qos_profile.history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
    qos_profile.depth = 1;

    auto node = rclcpp::Node::make_shared("test_publisher");
    auto publisher = node->create_publisher<test_messages::msg::TimestampMsg>("test_topic", qos_profile);
    auto message = std::make_shared<test_messages::msg::TimestampMsg>();

    auto publish_count = 0;
    rclcpp::WallRate loop_rate(PUBLISH_FREQUENCY);

    usleep(1000000);
    int i;
    for (i = 0; i < NBR_OF_MESSAGES; i++) {
        struct timespec ts;
        clock_gettime(CLOCK_MONOTONIC, &ts);
        std::cout << "I sent message nbr: " << publish_count << std::endl;
        message->sec = ts.tv_sec;
        message->nsec = ts.tv_nsec;
        message->seq = publish_count;
        if (i == NBR_OF_MESSAGES-1)
            message->last_msg = true;
        else
            message->last_msg = false;
        publisher->publish(message);
        publish_count++;

        rclcpp::spin_some(node);
        loop_rate.sleep();
    }

    rclcpp::shutdown();
    return 0;
}