#include <iostream>
#include <list>
#include <fstream>
#include "rclcpp/rclcpp.hpp"
#include <rclcpp/strategies/message_pool_memory_strategy.hpp>
#include "std_msgs/msg/int64.hpp"
#include "test_messages/msg/timestamp_msg.hpp"

int nbr_of_messages;
double total_latency;
double highest_latency, lowest_latency;
std::list<double> latencies;

void print_results(int msgs_out_of_order) {
	std::cout << "Done listening." << std::endl;
	std::cout << "I got " << nbr_of_messages << " messages, of which " << msgs_out_of_order << " were out of order." << std::endl;
	std::cout << "The average latency was " << total_latency / nbr_of_messages << " s." << std::endl;
	std::cout << "The lowest latency was " << lowest_latency << " s, and the highest was " << highest_latency << " s." << std::endl;
	std::cout << "Writing to latencies.txt" << std::endl;
	std::ofstream myfile;
	myfile.open("latencies.txt");
	for (double entry : latencies) {
		myfile << entry << ";";
	}
	myfile.close();
	std::cout << "Done writing to file." << std::endl;
}

void callback_function(const test_messages::msg::TimestampMsg::SharedPtr msg) {
	static struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);

	nbr_of_messages++;

	static int msgs_out_of_order = 0;
	static int highest_seq = 0;
	if (msg->seq < highest_seq)
		msgs_out_of_order++;
	else
		highest_seq = msg->seq;

	double timestamp = msg->sec + msg->nsec / 1000000000.0;
	std::cout << "I got message nbr: " << msg->seq << " and timestamp: " << timestamp << std::endl;

	double latency = ts.tv_sec + ts.tv_nsec / 1000000000.0 - timestamp;
	latencies.push_back(latency);
	total_latency += latency;
	if (latency > highest_latency)
		highest_latency = latency;
	if (latency < lowest_latency)
		lowest_latency = latency;

	if (msg->last_msg) {
		print_results(msgs_out_of_order);
		exit(0);
		/*total_latency = highest_latency = 0.0;
		lowest_latency = 1000000.0;
		nbr_of_messages = 0;*/
	}
}

int main(int argc, char** argv) {
	rclcpp::init(argc, argv);
	total_latency = highest_latency = 0.0;
	lowest_latency = 1000000.0;
	nbr_of_messages = 0;

	std::cout << "Subscriber node started." << std::endl;

	auto msg_strategy = std::make_shared<rclcpp::strategies::message_pool_memory_strategy::MessagePoolMemoryStrategy<test_messages::msg::TimestampMsg, 1>>();

	rmw_qos_profile_t qos_profile = rmw_qos_profile_default;
	qos_profile.reliability = RMW_QOS_POLICY_RELIABILITY_BEST_EFFORT;
	qos_profile.history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
	qos_profile.depth = 1;

	auto node = rclcpp::Node::make_shared("test_subscriber");
	auto subscription = node->create_subscription<test_messages::msg::TimestampMsg>(
		"test_topic", callback_function, qos_profile, nullptr, false, msg_strategy);
	rclcpp::spin(node);

	rclcpp::shutdown();
	return 0;
}